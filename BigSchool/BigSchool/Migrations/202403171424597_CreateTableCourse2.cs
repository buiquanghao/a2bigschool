﻿namespace BigSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTableCourse2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Courses", "LecturerID", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Courses", "LecturerID");
            AddForeignKey("dbo.Courses", "LecturerID", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            DropColumn("dbo.Courses", "Lecturer");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Courses", "Lecturer", c => c.String());
            DropForeignKey("dbo.Courses", "LecturerID", "dbo.AspNetUsers");
            DropIndex("dbo.Courses", new[] { "LecturerID" });
            AlterColumn("dbo.Courses", "LecturerID", c => c.String(nullable: false));
        }
    }
}
