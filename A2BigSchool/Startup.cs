﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(A2BigSchool.Startup))]
namespace A2BigSchool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
