﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace A2BigSchool.Models
{
    public class Category
    {
        public byte Id { get; set; }
        [Required]
        [StringLenght(255)]
        public string Name { get; set; }
    }
}